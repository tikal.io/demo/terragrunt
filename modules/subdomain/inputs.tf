variable "root_domain" {
  description = "The root domain's name e.g example.tikalk.dev"
  default = ""
}

variable "sub_domain_prefix" {
  description = "The sub domain's name e.g prod.exmaple.tikalk.dev"
  default = ""
}
