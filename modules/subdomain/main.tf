locals {
  root_domain = var.root_domain
  domain_name = "${var.sub_domain_prefix}.${var.root_domain}"
}

data "aws_route53_zone" "root" {
  name = local.root_domain
}

resource "aws_route53_zone" "main" {
  name          = local.domain_name
  force_destroy = "false"
  comment       = "Zone for ${local.domain_name}"
}

resource "aws_route53_record" "group-NS" {
  zone_id = "${data.aws_route53_zone.root.id}"
  name    = local.domain_name
  type    = "NS"
  ttl     = "900"

  records = [
    aws_route53_zone.main.name_servers.0,
    aws_route53_zone.main.name_servers.1,
    aws_route53_zone.main.name_servers.2,
    aws_route53_zone.main.name_servers.3,
  ]

  allow_overwrite = true
}
