locals {
    project = "camp"
    vendor_name = "tikalk"
    vendor_domain = "${local.vendor_name}.dev"
    project_domain = "${local.project}.${local.vendor_domain}"
}
