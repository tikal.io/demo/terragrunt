# Set account-wide variables. These are automatically pulled in to configure the remote state bucket in the root
# terragrunt.hcl configuration.
locals {
  account_name   = "tikal-infra"
  aws_account_id = "557680788250"

  # The aws profile to activate

  operators = [
    "hagzag@tikalk.com",
    "itaio@tikalk.com",
    "guyk@tikalk.com",
  ]

  gitlab_project_id = "11663448"
  gitlab_group_name = "TikalProCamp"

}