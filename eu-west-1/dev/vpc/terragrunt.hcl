# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}


locals {
  account = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  proj    = read_terragrunt_config(find_in_parent_folders("proj.hcl"))
  
  # account
  account_name = local.account.locals.account_name
  account_id   = local.account.locals.aws_account_id
  operators = local.account.locals.operators

  # region
  aws_region   = local.region.locals.aws_region

  # env
  environment = local.env.locals.environment
  vpc_cidr = local.env.locals.vpc_cidr
  
  # project
  project       = local.proj.locals.project
  vendor_name   = local.proj.locals.vendor_name
  vendor_domain = local.proj.locals.vendor_domain

  # assign current module locals
  domain_name       = "${local.project}.${local.vendor_domain}"
  k8s_cluster_name  = "${local.environment}-${replace(local.vendor_domain, ".", "-")}"
  namespace         = "${local.project}-${local.env.locals.environment}"
  cluster_name      = "${local.project}-${local.env.locals.environment}"

  }

dependency "data" {
  config_path = "${get_parent_terragrunt_dir()}/${local.aws_region}/${local.environment}/data"
}

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-vpc?ref=v2.47.0"
}

inputs = {
  name = local.namespace
  cidr = local.vpc_cidr

  azs             = dependency.data.outputs.availability_zones
  private_subnets = [cidrsubnet(local.vpc_cidr, 3, 0), cidrsubnet(local.vpc_cidr, 3, 1), cidrsubnet(local.vpc_cidr, 3, 2)]
  public_subnets  = [cidrsubnet(local.vpc_cidr, 3, 3), cidrsubnet(local.vpc_cidr, 3, 4), cidrsubnet(local.vpc_cidr, 3, 5)]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true


  # Cloudwatch log group and IAM role will be created
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true

  vpc_flow_log_tags = {
    Name = "${local.namespace}-vpc-flow-logs-cloudwatch-logs"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }


  tags = {
    Terraform = "true"
    Environment = local.namespace
  }

}
