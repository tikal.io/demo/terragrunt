include {
  path = find_in_parent_folders()
}

locals {
  env          = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  proj         = read_terragrunt_config(find_in_parent_folders("proj.hcl"))
  
  environment  = local.env.locals.environment
  root_domain  = local.proj.locals.project_domain
}


terraform {
  source = "${get_parent_terragrunt_dir()}/modules/subdomain/"
}

inputs = {
  sub_domain_prefix     =  local.environment
  root_domain           =  local.root_domain
}