# Bootstrap | Terragrunt Sanity module

Purpose of this module is to be small, potentially not use anything by initialize [providers]() and decalre some tags ;), a successful run should potentially check variable interpolation for the module and enable the end user to check if something is wrong with his setup.

## Usage

Create basic tags (using community module `cloudposse/terraform-null-label`) e.g:

```hcl
# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

locals {
  account = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  proj    = read_terragrunt_config(find_in_parent_folders("proj.hcl"))
  
  # account
  account_name     = local.account.locals.account_name
  account_id       = local.account.locals.aws_account_id
  operators        = local.account.locals.operators

  # region
  aws_region       = local.region.locals.aws_region

  # env
  environment      = local.env.locals.environment
  domain_name      = local.env.locals.domain_name
  k8s_cluster_name = local.env.locals.k8s_cluster_name
  
  namespace        = "${local.proj.locals.project}-${local.env.locals.environment}"
  cluster_name     = "${local.proj.locals.project}-${local.env.locals.environment}"

  }


terraform {
  source =  "git::https://github.com/cloudposse/terraform-null-label.git?ref=master"
}


inputs = {
  name       = local.namespace
  namespace  = local.namespace
  stage      = local.env
  attributes = ["public", "customer_facting"]
  delimiter  = "-"

  tags = {
    "BusinessUnit" = "Operations",
    "ClusterName" = local.k8s_cluster_name
  }
}

```