# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

locals {
  account = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  proj    = read_terragrunt_config(find_in_parent_folders("proj.hcl"))
  
  # account
  account_name = local.account.locals.account_name
  account_id   = local.account.locals.aws_account_id
  operators = local.account.locals.operators

  # region
  aws_region   = local.region.locals.aws_region

  # env
  environment = local.env.locals.environment
  
  # project
  project       = local.proj.locals.project
  vendor_name   = local.proj.locals.vendor_name
  vendor_domain = local.proj.locals.vendor_domain

  # assign current module locals
  domain_name       = "${local.project}.${local.vendor_domain}"
  k8s_cluster_name  = "${local.environment}-${replace(local.vendor_domain, ".", "-")}"
  namespace         = "${local.project}-${local.env.locals.environment}"
  cluster_name      = "${local.project}-${local.env.locals.environment}"

  }


terraform {
}

