data "aws_availability_zones" "all" {}

output "availability_zones" {
  value = data.aws_availability_zones.all.names
}