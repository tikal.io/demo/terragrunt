# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}


locals {
  account = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  proj    = read_terragrunt_config(find_in_parent_folders("proj.hcl"))
  
  # Extract the variables we need for easy access
  account_name = local.account.locals.account_name
  account_id   = local.account.locals.aws_account_id
  aws_region   = local.region.locals.aws_region
  
  namespace    = "${local.proj.locals.project}-${local.env.locals.environment}"

  cluster_name = "${local.proj.locals.project}-${local.env.locals.environment}"
  environment  = local.env.locals.environment


  }


dependency "kubernetes" {
  config_path = "${get_parent_terragrunt_dir()}/${local.account_name}/${local.environment}/${local.aws_region}/kubernetes"
} 


terraform {
}


inputs = {
  k8s_service_account_namespace = "kube-system"
  k8s_service_account_name = "external-dns"
  cluster_id = dependency.kubernetes.outputs.cluster_id
  provider_url = replace(dependency.kubernetes.outputs.cluster_oidc_issuer_url, "https://", "")
}
