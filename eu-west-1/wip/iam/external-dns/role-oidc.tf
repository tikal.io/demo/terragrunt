resource "aws_iam_policy" "role" {
  name_prefix = var.k8s_service_account_name
  description = "EKS role policy for cluster ${var.cluster_id}"
  policy      = data.aws_iam_policy_document.role.json
}

data "aws_iam_policy_document" "role" {
  statement {
    sid = "ChangeResourceRecordSets"

    actions = [
      "route53:ChangeResourceRecordSets",
    ]

    resources = [ "*" ]

    effect = "Allow"
  }

  statement {
    sid = "ListResourceRecordSets"

    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets",
      "route53:ListTagsForResource",
    ]

    resources = [
      "*",
    ]

    effect = "Allow"
  }
}

module "iam_assumable_role_admin" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "2.14.0"
  create_role                   = true
  role_name                     = var.k8s_service_account_name
  provider_url                  = var.provider_url
  role_policy_arns              = [aws_iam_policy.role.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:${var.k8s_service_account_namespace}:${var.k8s_service_account_name}"]
}


output "policy_arn" {
    value = aws_iam_policy.role.arn
}

output "role_arn" {
  value       = module.iam_assumable_role_admin.this_iam_role_arn
}